import { Home, Gallery, Intro, Breeds, Adoption } from './models/home';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public home: Home;
  public gallery: Gallery;
  public intro: Intro;
  public breeds: Breeds;
  public adoption: Adoption;
  public inputText: string = '';
  public welcomeMessage: string ='';
  constructor() {
    this.intro = {
      welcomeQuestion: 'First of all. What\'s your name?',
      introImg: {
        img: 'https://pngarchive.com/public/uploads/small/1001568067390kiqxqbovm9wdp4b5wfrkqyewbs7r64jhmyunpboahr2mixrdbclz14zhvmcg0uluk1o837a4qbfr3n0opyoqaefyj2b3obs0uyhe.png',
        name: 'Long Black Cat'
      },
      introDescription: 'This webpage is dedicated to cats. Devoted to cats. Trully committed.'
    };
    this.breeds = {
      breedsTitle:'These are all known breeds of cats',
      breed: [
        {
          breedName: 'European Shorthair',
          breedImg:{
            img:'https://upload.wikimedia.org/wikipedia/commons/5/5d/European_shorthair_procumbent_Quincy.jpg',
            name:'European Shorthair Cat loooking fancy'
          },
          breedDescription:'European Shorthair cats are the ones you\'ll usually see, they\'re everywhere!'
        },
        {
          breedName: 'Bengal Cat',
          breedImg:{
            img:'https://i.pinimg.com/originals/76/39/ff/7639ff24def03e6fe552e65d45cee8f2.jpg',
            name:'Really fancy bengal cat'
          },
          breedDescription:'These are the fanciest cats ever, look at them! Sooooo cool.'
        },
      ],
      adoption: {
        adoptionTitle: 'Would you like to addopt this kitty?',
        adoptionStatus: ''
      }
    };
    this.gallery = {
      galleryTitle:'These are all our cats!',
      galleryImg: [
        {
          img: 'https://catbreedsfaq.com/wp-content/uploads/2020/09/5-cat-breeds-on-white-background.png',
          name: 'Some cats'
        },
        {
          img: 'https://www.petbusinessworld.co.uk/UserFiles/Image/Trade%20News%20Pics/2020/05%20may/kittens_full3.png',
          name: 'Some small cats'
        },
      ]
    }
    this.home = {
      intro: this.intro,
      breeds: this.breeds,
      gallery: this.gallery
    };
  }


  ngOnInit(): void {
  }

  setMessage(message: string): void {
    this.welcomeMessage = 'Welcome, ' + message + '!';
  }
}