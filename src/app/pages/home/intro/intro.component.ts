import { Component, Input, OnInit , Output, EventEmitter } from '@angular/core';
import { Intro } from '../models/home';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
@Input() intro!: Intro;
@Output() emitMessage = new EventEmitter<string>();
message: string = '';
  constructor() { }

  ngOnInit(): void {
  }

  sendMessage() {
    this.emitMessage.emit(this.message);
  }
}
