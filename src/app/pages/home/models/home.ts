export interface Home {
    intro: Intro;
    breeds: Breeds;
    gallery: Gallery;
}

export interface Intro{
    welcomeQuestion: string,
    introImg: Image;
    introDescription: string;
}

export interface Breeds {
    breedsTitle: string,
    breed: Array<BreedInfo>;
    adoption: Adoption;
}

export interface Gallery {
    galleryTitle: string;
    galleryImg: Array<Image>;
}


export interface Image {
    img: string;
    name: string;
}

export interface BreedInfo {
    breedName: string;
    breedImg: Image;
    breedDescription: string;
}

export interface Adoption {
    adoptionTitle: string;
    adoptionStatus: string;
}
