import { Component, OnInit, Input } from '@angular/core';
import { Gallery } from '../models/home';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  @Input() gallery!: Gallery;
  constructor() { }

  ngOnInit(): void {
  }

}
