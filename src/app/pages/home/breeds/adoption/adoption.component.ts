import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Adoption } from '../../models/home';

@Component({
  selector: 'app-adoption',
  templateUrl: './adoption.component.html',
  styleUrls: ['./adoption.component.scss']
})
export class AdoptionComponent implements OnInit {
@Input() adoption: Adoption;
@Output() emitAdoptionMessage = new EventEmitter<string>();
adoptionStatusMessage: string = 'Kitty adopted!';
  constructor() { }

  ngOnInit(): void {
  }

  sendAdoptionMessage() {
    this.emitAdoptionMessage.emit(this.adoptionStatusMessage);
  }
}
