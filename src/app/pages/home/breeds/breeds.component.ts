import { Component, OnInit, Input } from '@angular/core';
import { Breeds } from '../models/home';

@Component({
  selector: 'app-breeds',
  templateUrl: './breeds.component.html',
  styleUrls: ['./breeds.component.scss']
})
export class BreedsComponent implements OnInit {
  @Input() breeds!: Breeds;
  public flag:boolean;
  public adoptionStatusMessage: string;
  constructor() {
    this.flag = false;
    this.adoptionStatusMessage = "";
  }

  ngOnInit(): void {
  }

  onButtonClick() : void {
    this.flag = !this.flag;
  }

  setAdoptionMessage(adoptionStatusMessage: string): void {
    this.adoptionStatusMessage = adoptionStatusMessage;
  }

}
