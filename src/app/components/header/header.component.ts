import { Component, OnInit } from '@angular/core';
import { Header } from './models/header';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public header:Header;
  constructor() {
    this.header = {
      icon: {
        iconImg: 'https://i.pinimg.com/originals/fe/e3/a7/fee3a76abad7cfae3d3a62c29c413d75.png',
        iconName: 'Cat Logo'
      },
      headerTitle: {
        mainTitle: 'CATS!',
        subtitle: 'Those fluffy bastards'
      },
      links: [
        {
          linkIcon: {
            iconImg: '',
            iconName: ''
          },
          linkText: 'Main',
          linkHref:'Main'
        },
        {
          linkIcon: {
            iconImg: '',
            iconName: ''
          },
          linkText: 'Gallery',
          linkHref:'Gallery'
        },
        {
          linkIcon: {
            iconImg: '',
            iconName: ''
          },
          linkText: 'Breeds',
          linkHref:'Breeds'
        }
      ]
    }
  }

  ngOnInit(): void {
  }

}
