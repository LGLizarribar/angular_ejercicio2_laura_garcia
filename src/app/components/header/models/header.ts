export interface Header {
    icon: Icon;
    headerTitle: Title;
    links: Array<Link>;
}

export interface Icon {
    iconImg: string;
    iconName: string;
}

export interface Link {
    linkIcon: Icon;
    linkText: string;
    linkHref: string;
}

export interface Title {
    mainTitle: string;
    subtitle: string;
}

