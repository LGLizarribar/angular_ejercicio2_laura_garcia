import { Component, OnInit } from '@angular/core';
import { Footer } from './models/footer';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public footer:Footer;
  constructor() {
    this.footer = {
      links: [
      {
        linkText: 'About Cats',
        linkHref: ''
      },
      {
        linkText: 'Cat\'s Rights',
        linkHref: ''
      },
      {
        linkText: 'Donate for Cats',
        linkHref: ''
      },
    ]}
  }

  ngOnInit(): void {
  }

}
